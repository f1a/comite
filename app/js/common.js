$(function() {
//Navbar Script
	const navbar = document.getElementById("navbar");
const navbarToggle = navbar.querySelector(".navbar-toggle");

function openMobileNavbar() {
  navbar.classList.add("opened");
  navbarToggle.setAttribute("aria-label", "Close navigation menu.");
}

function closeMobileNavbar() {
  navbar.classList.remove("opened");
  navbarToggle.setAttribute("aria-label", "Open navigation menu.");
}

navbarToggle.addEventListener("click", () => {
  if (navbar.classList.contains("opened")) {
    closeMobileNavbar();
  } else {
    openMobileNavbar();
  }
});

const navbarMenu = navbar.querySelector(".navbar-menu");
const navbarLinksContainer = navbar.querySelector(".navbar-links");

navbarLinksContainer.addEventListener("click", (clickEvent) => {
  clickEvent.stopPropagation();
});

navbarMenu.addEventListener("click", closeMobileNavbar);

// Input checkbox toggle
$('.inputs.guests').children('span').click(function(e){
	e.preventDefault();
	$(this).parent().toggleClass('active');
})

//form1
$('.form1 input').change(function () {
	let form1_trigger = true;
	$('.form1 input').each(function () {
		if($(this).val().length==0){
			form1_trigger = false;
		}
	}).promise().done( function(){ 
		if(form1_trigger == true){
			$('.form1 button').removeAttr('disabled');
		}else{
			
			$('.form1 button').attr({'disabled':''});
		}
	 } );
	
})
$('.form1 button').click(function(e){
	e.preventDefault();
	$('.form2').slideDown();
	$('.pay').slideDown();
	$('html, body').animate({scrollTop: $(".form2").offset().top}, 2000);
})
const _elem = $('.form2 .input_cont').children('.guests').html();
$('.controls .left a').click(function (e) {
	e.preventDefault();
	if ( $('.form2 .input_cont').children('.guests').length<10){
		$('.form2 .input_cont').prepend('<div class="inputs guests">'+_elem+'</div>')
	}
	$('.inputs.guests').children('span').click(function(e){
		e.preventDefault();
		$(this).parent().toggleClass('active');
	})
})
$('.controls .right a').click(function (e) {
		e.preventDefault();
		$('.form2 .input_cont .guests.active').each(function () {
			$(this).remove();
			
		});
	})
});
